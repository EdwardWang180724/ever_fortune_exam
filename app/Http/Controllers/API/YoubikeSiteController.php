<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\controller;
use App\Models\YoubikeSite;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;

class YoubikeSiteController extends Controller{

    public $successStatus = 200;

    public function index(Request $request){
        $perPage = $request->input('per_page', 10);

        // select data
        $youbikeSiteModel = YoubikeSite::
            select(["sno", "sna", "tot", "sbi", "sarea", "mday", "lat", "lng", "ar", "sareaen", "snaen",
                "aren", "bemp", "act"])
            ->selectRaw("(SELECT COUNT(*) FROM youbike_comment WHERE youbike_site_sno = youbike_site.sno) AS comment_count")
            ;

        // search name
        $siteName = $request->input('site_name');
        if (isset($siteName)) {
            $youbikeSiteModel->where(function($query) use($siteName) {
                $query->where('sna', 'like', '%' . $siteName . '%')
                    ->orWhere('snaen', 'like', '%' . $siteName . '%');
            });
        }

        // search area
        $area = $request->input('area');
        if (isset($area)) {
            $youbikeSiteModel->where(function($query) use($area) {
                $query->where('ar', 'like', '%' . $area . '%')
                    ->orWhere('aren', 'like', '%' . $area . '%');
            });
        }

        // no bikes
        $noBikes = $request->input('no_bikes');
        if ($noBikes == 1) {
            $youbikeSiteModel->where('bemp', 0);
        }

        $youbikeSites = $youbikeSiteModel->paginate($perPage);
//        echo "youbikeSites = <pre>"; print_r($youbikeSites); echo "</pre>";

        return response()->json([
            'result'=> $youbikeSites
        ], $this->successStatus);
    }

}
