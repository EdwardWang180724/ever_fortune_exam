<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\YoubikeComment;
use App\Models\YoubikeSite;
use Illuminate\Http\Request;
use Auth;
use Validator;

class YoubikeCommentController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 驗證
        $validator = Validator::make($request->all(), [
            'youbike_site_sno' => ['required'],
            'description' => ['required', 'min:1', 'max:500'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages(),
                'error' => 'validate error.'
            ], 401);
        }

        $youbikeSiteSno = $request->input('youbike_site_sno');
        $description = $request->input('description');

        // check exist
        $youbikeSite = YoubikeSite::where('sno', $youbikeSiteSno)->first();
        if (!isset($youbikeSite)) {
            return response()->json([
                'message' => 'youbike site sno does not exist.',
                'error' => 'validate error.'
            ], 401);
        }

        // insert data
        $youbikeComment = new YoubikeComment();
        $youbikeComment->youbike_site_sno = $youbikeSiteSno;
        $youbikeComment->user_id = Auth::guard('api')->user()->id;
        $youbikeComment->description = $description;
        $youbikeComment->save();

        return response()->json([
            'success' => ["message" => 'OK']
        ], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 驗證
        $validator = Validator::make($request->all(), [
            'description' => ['required', 'min:1', 'max:500'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages(),
                'error' => 'validate error.'
            ], 401);
        }

        $description = $request->input('description');

        // update
        YoubikeComment::where('id', $id)
            ->where('user_id', Auth::guard('api')->user()->id)
            ->update([
                'description' => $description
            ]);

        return response()->json([
            'success' => ["message" => 'OK']
        ], $this->successStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        YoubikeComment::where('id', $id)
            ->where('user_id', Auth::guard('api')->user()->id)
            ->delete();

        return response()->json([
            'success' => ["message" => 'OK']
        ], $this->successStatus);
    }
}
