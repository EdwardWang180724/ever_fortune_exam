<?php


namespace App\Crawlers;


use App\Models\YoubikeSite;
use GuzzleHttp\Client;

class YoubikeCrawler
{
    /**
     * 執行擷取儲存youbike資料
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function run() {
        $client = new Client();
        $res = $client->request('GET', 'https://tcgbusfs.blob.core.windows.net/blobyoubike/YouBikeTP.json', []);

        $jsonRes = $res->getBody();

        $jsonDecodes = json_decode($jsonRes);
//        echo "<pre>"; print_r($jsonDecodes); echo "</pre>";

        foreach($jsonDecodes->retVal as $jsonDecode) {
            YoubikeSite::firstOrCreate(
                ['sno' => $jsonDecode->sno],
                [
                    'sna' => $jsonDecode->sna,
                    'tot' => $jsonDecode->tot,
                    'sbi' => $jsonDecode->sbi,
                    'sarea' => $jsonDecode->sarea,
                    'mday' => $jsonDecode->mday,
                    'lat' => $jsonDecode->lat,
                    'lng' => $jsonDecode->lng,
                    'ar' => $jsonDecode->ar,
                    'sareaen' => $jsonDecode->sareaen,
                    'snaen' => $jsonDecode->snaen,
                    'aren' => $jsonDecode->aren,
                    'bemp' => $jsonDecode->bemp,
                    'act' => $jsonDecode->act
                ]
            );
        }
    }
}
