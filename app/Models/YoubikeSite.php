<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YoubikeSite extends Model
{
    protected $table = 'youbike_site';
    protected $guarded = [];
}
