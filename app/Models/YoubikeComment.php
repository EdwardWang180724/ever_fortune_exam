<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YoubikeComment extends Model
{
    protected $table = 'youbike_comment';
    protected $guarded = [];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function youbikeSite()
    {
        return $this->belongsTo(YoubikeSite::class, "youbike_site_sno", "sno");
    }
}
