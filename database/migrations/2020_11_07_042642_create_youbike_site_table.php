<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoubikeSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youbike_site', function (Blueprint $table) {
            $table->string('sno', 10);
            $table->string("sna");
            $table->integer("tot");
            $table->integer("sbi");
            $table->string("sarea");
            $table->string("mday");
            $table->string("lat");
            $table->string("lng");
            $table->string("ar");
            $table->string("sareaen");
            $table->string("snaen");
            $table->string("aren");
            $table->integer("bemp");
            $table->integer("act");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youbike_site');
    }
}
