<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoubikeCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youbike_comment', function (Blueprint $table) {
            $table->id();
            $table->string('youbike_site_sno', 10);
            $table->bigInteger('user_id')->unsigned();
            $table->text("description");
            $table->timestamps();

//            $table->foreign('youbike_site_sno')->references('sno')->on('youbike_site')->onDelete('cascade');
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youbike_comment');
    }
}
